# Chapter 7: Credit system

Workers, in exchange for their labor, receive credits. After a labor record has been finalized by the company, credits are added to that worker's Basis account balance.

Credits can be spent on goods and services provided by companies in the Basis system, and are destroyed on spending. Credits do not expire, and are transferable to other members freely.

## Labor and resources

Because Basis tracks costs of products and services in terms of labor and resources, it makes sense that the credits people can spend in exchange for those products and services would be denominated in labor and resources.

Credits will have a negotiable multiplier. For instance, if a doctor has a multiplier of 9 and she works 5 hours, she will get 45 credits.

The labor content of credits will always be based on hours of work completed. The resource content of resources will be a globally-decided (democratically) set of resources that reflect the resource usage plan of the system. For instance, if the world's iron reserves are estimated to be depleted in 10 years, the members of the system might vote to reduce the iron content of the resource credits, thus slowly weening off iron usage. Or if, hypothetically, some sort of climate disaster caused by unearthing some resource and burning it in excess runs rampant, the members of the system might vote to limit the resources responsible for that hypothetical climate problem.

## Currency exchange

Exchange between credits and national currency will be possible through the regional bank. This bank will have a pool of capital specifically used to back the value of the credits. Members can convert their credits into currency (say, USD), however credit exchange is one-way: *credits will never be issued in exchange for currency*. The only way for new credits to be issued is to perform labor in the system! This prevents speculation on the market value of labor credits and protects members from sudden changes in value caused by external forces.

